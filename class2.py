class SM:
    '''dafd'''
    count = 0
    def __init__(self,name,age):
        self.name = name
        self.age = age
        SM.count+=1
        print("init SM")

    def tell(self):
        '''tel'''
        print('''name:"%s" age:"%s"''' %(self.name,self.age),end="")

class Teacher(SM):
    '''Represents a teacher.'''
    def __init__(self,name,age,salary):
        SM.__init__(self,name,age)
        self.salary = salary
        print('(Initialized Teacher: %s)' %self.name)
    def tell(self):
        SM.tell(self)
        print(',Salary: "%d"' %self.salary)
class Student(SM):
    '''Represents a student.'''
    def __init__(self,name,age,marks):
        SM.__init__(self,name,age)
        self.marks = marks
        print('(Initialized Student: %s)' %self.name)
    def tell(self):
        SM.tell(self)
        print('Marks: "%d"' %self.marks)

t =  Teacher("tt",32,4000)
s = Student("ss",12,87)

print()
print(SM.count)
t.tell()
s.tell()