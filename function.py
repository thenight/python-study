def sum(a,b):
    return (a+b)

func = sum

r = func(5,6)

print(r)

#默认值
def add(a,b=2):
    return a+b
r = add(1)
print(r)

#3之前range返回的是一个列表，3之后返回一个迭代，所以要加list()
a = list(range(5,10))
print(a)

for x in range(4,9):
    print(x,end="")#打印不换行
