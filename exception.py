s = "a"

if s == "":
    raise Exception("is empty")

try:
    i = int(s)
except ValueError:
    print("不能转换")
except:
    print("Unknown exception")
#没有捕获到异常
else:
    print("out:%d"%i)
finally:
    print("finally")
