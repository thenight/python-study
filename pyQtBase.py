import sys
from PyQt4 import QtCore,QtGui
class MyWin(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.setWindowTitle("pyqt")
        self.resize(300,200)
        label = QtGui.QLabel("label")
        label.setAlignment(QtCore.Qt.AlignCenter)
        self.setCentralWidget(label)
app = QtGui.QApplication(sys.argv)
win = MyWin()
win.show()
app.exec_()