from tkinter import *

class App:
    def __init__(self,master):
        frame = Frame(master)
        frame.pack()
        self.button = Button(frame,text="quick",fg="red",command=frame.quit)
        self.button.pack(side=LEFT)
        self.hi = Button(frame,text="hi",command=self.say)
        self.hi.pack(side=LEFT)

    def say(self):
        print("hi")

win = Tk()
app = App(win)
win.mainloop()